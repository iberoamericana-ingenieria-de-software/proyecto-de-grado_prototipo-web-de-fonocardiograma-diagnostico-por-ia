from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
from flask_cors import CORS
import os
from werkzeug.utils import secure_filename
import json
from flask import send_from_directory, abort
import tensorflow as tf
import numpy as np
from PIL import Image
import cv2
import uuid
from datetime import datetime

app = Flask(__name__)
CORS(app)
CORS(app, origins='*')

# Configura la conexión a MongoDB
app.config['MONGO_URI'] = 'mongodb://localhost:27017/fncdatabase'
mongo = PyMongo(app)
audio_data = mongo.db.audio_data

UPLOAD_FOLDER = 'C:/Users/jacop/Documents/prueba/fonocardiogramaDos/myfnc/BackEndFnc/audiofiles'
IMG_UPLOAD_FOLDER = 'C:/Users/jacop/Documents/prueba/fonocardiogramaDos/myfnc/BackEndFnc/imgfiles'
ALLOWED_EXTENSIONS_AUDIO = {'wav'}
ALLOWED_EXTENSIONS_IMAGE = {'jpg'}



def allowed_file(filename, allowed_extensions):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allowed_extensions


app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['IMG_UPLOAD_FOLDER'] = IMG_UPLOAD_FOLDER

def allowed_file(filename, allowed_extensions):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in allowed_extensions

@app.route('/capture', methods=['POST'])
def save_data():
    try:
        data = json.loads(request.form['data'])

        nombreCompleto = data.get('nombreCompleto')
        tipoIdentificacion = data.get('tipoIdentificacion')
        identification = data.get('identification')
        edad = data.get('edad')
        sexoBiologico = data.get('sexoBiologico')
        focoAuscultatorio = data.get('focoAuscultatorio')

        # Generar el nombre del archivo de audio con el formato deseado
        current_datetime = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        filename = f"{identification}_{focoAuscultatorio}_{current_datetime}.wav"

        # Guardar el archivo de audio en el servidor
        audio_file = request.files['audio']
        if audio_file and allowed_file(audio_file.filename, {'wav'}):
            audio_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            audio_file.save(audio_path)
        else:
            return jsonify({'error': 'Archivo de audio no válido'}), 400

        audio_data.insert_one({
            'nombreCompleto': nombreCompleto,
            'tipoIdentificacion': tipoIdentificacion,
            'identification': identification,
            'edad': edad,
            'sexoBiologico': sexoBiologico,
            'focoAuscultatorio': focoAuscultatorio,
            'audio': audio_path  # Guardar la ruta del archivo de audio en la base de datos
        })

        return jsonify({'message': 'Data saved successfully'}), 200
    except Exception as e:
        app.logger.error('Error al procesar la solicitud POST: %s', e)
        return jsonify({'error': str(e)}), 500

@app.route('/upload_image', methods=['POST'])
def upload_image():
    try:
        if 'image' not in request.files:
            return jsonify({'error': 'No image part'}), 400
        
        file = request.files['image']
        
        if file.filename == '':
            return jsonify({'error': 'No selected file'}), 400
        
        if file and allowed_file(file.filename, ALLOWED_EXTENSIONS_IMAGE):
            filename = secure_filename(file.filename)
            # Generar un nombre único para el archivo
            unique_filename = str(uuid.uuid4()) + '_' + filename
            file_path = os.path.join(app.config['IMG_UPLOAD_FOLDER'], unique_filename)
            file.save(file_path)
            return jsonify({'message': 'Image uploaded successfully', 'filename': unique_filename}), 200
        else:
            return jsonify({'error': 'Invalid file type'}), 400
    except Exception as e:
        app.logger.error('Error al subir la imagen: %s', e)
        return jsonify({'error': str(e)}), 500

@app.route('/modelo', methods=['GET'])
def obtener_modelo():
    modelo_path = 'C:/Users/jacop/Documents/prueba/fonocardiogramaDos/myfnc/BackEndFnc/env/modelos/1'
    modelo_file = 'modelo_myfnc.h5'
    # Verifica si el archivo existe
    if os.path.exists(os.path.join(modelo_path, modelo_file)):
        return send_from_directory(modelo_path, modelo_file)
    else:
        abort(404, description="Archivo no encontrado")

# Cargar el modelo de TensorFlow al iniciar el servidor
modelo_path = 'C:/Users/jacop/Documents/prueba/fonocardiogramaDos/myfnc/BackEndFnc/env/modelos/1/modelo_myfnc.h5'
modelo = tf.keras.models.load_model(modelo_path)

def preparar_imagen(file):
    img = Image.open(file)
    img = img.convert('L')  # Convertir a escala de grises
    img = np.array(img).astype(float) / 255  # Normalizar valores de píxeles
    img = cv2.resize(img, (1000, 400))  # Redimensionar la imagen al tamaño esperado por el modelo
    img = np.expand_dims(img, axis=-1)  # Añadir una dimensión para el canal (escala de grises)
    img = np.expand_dims(img, axis=0)  # Añadir una dimensión para el lote
    return img

@app.route('/clasificar', methods=['POST'])
def clasificar_imagen():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'}), 400
    
    file = request.files['file']
    
    if file.filename == '':
        return jsonify({'error': 'No selected file'}), 400
    
    try:
        img = preparar_imagen(file)
        prediccion = modelo.predict(img)
        clase = np.argmax(prediccion, axis=-1)[0]
        
        return jsonify({'clasificacion': str(clase)}), 200
    except Exception as e:
        app.logger.error('Error al clasificar la imagen: %s', e)
        return jsonify({'error': str(e)}), 500

@app.route('/audiofiles', methods=['GET'])
def obtener_archivos_de_audio():
    try:
        # Filtrar archivos con la extensión permitida
        files = os.listdir(app.config['UPLOAD_FOLDER'])
        audio_files = [f for f in files if f.lower().endswith('.wav')]
        # Obtener la información de los archivos (nombre y fecha de modificación)
        audio_files_info = [{'nombre': f, 'fecha_modificacion': os.path.getmtime(os.path.join(app.config['UPLOAD_FOLDER'], f))} for f in audio_files]
        # Ordenar la lista de archivos por nombre y fecha de modificación
        audio_files_info_sorted = sorted(audio_files_info, key=lambda x: (x['nombre'], x['fecha_modificacion']))
        return jsonify(audio_files_info_sorted), 200
    except Exception as e:
        app.logger.error('Error al obtener los archivos de audio: %s', e)
        return jsonify({'error': str(e)}), 500

@app.route('/audiofiles/<filename>')
def get_audio_file(filename):
    try:
        return send_from_directory(app.config['UPLOAD_FOLDER'], filename)
    except Exception as e:
        app.logger.error('Error al enviar el archivo de audio: %s', e)
        return jsonify({'error': str(e)}), 500
    

@app.route('/imgfiles', methods=['GET'])
def obtener_archivos_de_imagen():
    try:
        img_folder = app.config['IMG_UPLOAD_FOLDER']
        files = os.listdir(img_folder)
        img_files = [f for f in files if f.lower().endswith(('.jpg'))]
        img_files_info = [{'nombre': f, 'fecha_modificacion': os.path.getmtime(os.path.join(img_folder, f))} for f in img_files]
        img_files_info_sorted = sorted(img_files_info, key=lambda x: (x['nombre'], x['fecha_modificacion']))
        return jsonify(img_files_info_sorted), 200
    except Exception as e:
        app.logger.error('Error al obtener los archivos de imagen: %s', e)
        return jsonify({'error': str(e)}), 500
    

@app.route('/imgfiles/<filename>')
def get_image_file(filename):
    try:
        return send_from_directory(app.config['IMG_UPLOAD_FOLDER'], filename)
    except Exception as e:
        app.logger.error('Error al enviar el archivo de imagen: %s', e)
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)