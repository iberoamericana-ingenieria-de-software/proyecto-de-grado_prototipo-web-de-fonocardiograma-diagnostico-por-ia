import React, { useState, useRef, useEffect } from 'react';
import CaptureSound from './componentes/CaptureSound.jsx';
import WaveformDisplay from './componentes/WaveformDisplay.jsx';
import ChartSt from './componentes/ChartSt.jsx';
import ImageDg from './componentes/ImageDg.jsx'; 
import FonoCardioTab from './componentes/fonoCardioTab.jsx';
import ImageClassifier from './componentes/ImageClassifier.jsx';
import './App.css';
import axios from 'axios';

function App() {
  const [showWaveform, setShowWaveform] = useState(false);
  const [selectedAudio, setSelectedAudio] = useState(null);
  const [waveformData, setWaveformData] = useState([]);
  const [audioFiles, setAudioFiles] = useState([]);
  const [showFileList, setShowFileList] = useState(false);
  const [selectedFileName, setSelectedFileName] = useState('');
  const chartRef = useRef(null); // Referencia para el canvas de ChartSt

  const toggleWaveform = () => {
    setShowWaveform(!showWaveform);
  };

  const getWaveformData = (data) => {
    setWaveformData(data);
  };

  const fetchAudioFiles = async () => {
    try {
      const response = await axios.get('http://localhost:5000/audiofiles'); // Cambia la URL según la ubicación de tu backend
      setAudioFiles(response.data);
    } catch (error) {
      console.error('Error al obtener archivos de audio:', error);
    }
  };

  useEffect(() => {
    fetchAudioFiles();
  }, []);

  const handleAudioFileSelect = async (audioFile) => {
    try {
      const response = await axios.get(`http://127.0.0.1:5000/audiofiles/${audioFile.nombre}`, {
        responseType: 'blob'
      });
      const file = new Blob([response.data], { type: 'audio/wav' });
      setSelectedAudio(URL.createObjectURL(file));
      setSelectedFileName(audioFile.nombre); // Set the selected file name
      setShowFileList(false); // Hide the file list after selection
      setShowWaveform(true); // Show the waveform after file selection
      
      // Actualizar la lista de archivos de audio después de la selección
      fetchAudioFiles();
    } catch (error) {
      console.error('Error al descargar el audio:', error);
    }
  };

  const [showFonoCardioTab, setShowFonoCardioTab] = useState(false);

  const toggleFonoCardioTab = () => {
    setShowFonoCardioTab(!showFonoCardioTab);
  };

  return (
    <div className="App">
      <div className="banner">
        <h1>Proyecto Kardipohon versión 1.0-prototipo mínimamente viable.</h1>
      </div>
      <h1>CAPTURAR SONIDO</h1>
      <CaptureSound />
      <h1>GRAFICAR</h1>
      <div>
        <h2>Elegir Archivo de Audio:</h2>
        <button type="button" onClick={() => setShowFileList(!showFileList)}>
          {selectedFileName ? `Seleccionado: ${selectedFileName}` : 'Elegir Archivo de Audio'}
        </button>
        {showFileList && (
          <ul>
            {audioFiles.map((audioFile, index) => (
              <li
                key={index}
                onClick={() => handleAudioFileSelect(audioFile)}
                className={selectedFileName === audioFile.nombre ? 'selected' : ''}
              >
                {audioFile.nombre} - {new Date(audioFile.fecha_modificacion).toLocaleString()}
              </li>
            ))}
          </ul>
        )}
      </div>
      <button className="button" onClick={toggleWaveform}>
        {showWaveform ? 'Ocultar Gráfica' : 'Mostrar Gráfica'}
      </button>
      {showWaveform && (
        <div className="wave">
          <WaveformDisplay audioSrc={selectedAudio} onDataExtracted={getWaveformData} />
          <ChartSt waveformData={waveformData} chartRef={chartRef} />
        </div>
      )}
      <div className="FonoCardioTab">
        <button className="button" onClick={toggleFonoCardioTab}>Datos</button>
        {showFonoCardioTab && <FonoCardioTab waveformData={waveformData} />}
        <ImageDg chartRef={chartRef} />
      </div>
      <h1>CLASIFICAR IMÁGENES</h1>
      <ImageClassifier />
      <div className="pie">
        <h1>Elaborado por: Jorge Alejandro Chamorro Ordoñez</h1>
      </div>
    </div>
  );
}

export default App;