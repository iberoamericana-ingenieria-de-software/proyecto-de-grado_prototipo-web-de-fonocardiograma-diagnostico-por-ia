import React, { useState, useEffect } from 'react';
import '../estilos/FonoCardioTab.css';

const FonoCardioTab = ({ waveformData }) => {
  const [captureDate, setCaptureDate] = useState('');
  const [duration, setDuration] = useState('');
  const [frequency, setFrequency] = useState('');

  useEffect(() => {
    if (waveformData && waveformData.length > 0) {
      // Calcular la fecha de captura (por ejemplo, la fecha actual)
      const currentDate = new Date();
      setCaptureDate(currentDate.toISOString());

      // Calcular la duración del audio en segundos
      const audioDuration = 10; // 8000 es un ejemplo de frecuencia de muestreo
      setDuration(audioDuration.toFixed(1));

      // Realizar cálculos para obtener la frecuencia (puedes usar cualquier algoritmo que se ajuste a tus necesidades)
      const calculatedFrequency = calcularFrecuencia(waveformData);
      setFrequency(calculatedFrequency);
    }
  }, [waveformData]);

  // Función de ejemplo para calcular la frecuencia
  const calcularFrecuencia = (waveformData) => {
    const amplitudes = waveformData; 
    const tiempoVentana = 10; 
    const frecuenciaMuestreo = 3; 
    const muestrasEnVentana = tiempoVentana * frecuenciaMuestreo;
    const maxAmplitud = Math.max(...amplitudes);
    const umbralPico = 0.70 * maxAmplitud;
    let picos = 0;
  
    for (let i = 0; i < amplitudes.length - muestrasEnVentana; i++) {
      const ventana = amplitudes.slice(i, i + muestrasEnVentana);
      const maxVentana = Math.max(...ventana);
  
      if (maxVentana >= umbralPico) {
        picos++;
        i += muestrasEnVentana;
      }
    }
  
    const frecuencia = picos*60 / tiempoVentana;
    return frecuencia.toFixed(1); 
  };


  return (
    <div>
      <h2>Datos del Audio</h2>
      <p>Fecha de Captura: {captureDate}</p>
      <p>Duración: {duration} segundos</p>
      <p>Frecuencia: {frequency}</p>
    </div>
  );
};

export default FonoCardioTab;