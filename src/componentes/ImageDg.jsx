import React from 'react';
import axios from 'axios';

const ImageDg = ({ chartRef }) => {
  const captureImage = async () => {
    const canvas = chartRef.current;
    if (canvas) {
      canvas.toBlob(async (blob) => {
        const formData = new FormData();
        formData.append('image', blob, 'captured_waveform.jpg');

        try {
          const response = await axios.post('http://localhost:5000/upload_image', formData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          });
          console.log('Image uploaded successfully:', response.data);
        } catch (error) {
          console.error('Error uploading image:', error);
        }
      }, 'image/jpg');
    } else {
      console.error('Canvas reference not found.');
    }
  };

  return (
    <div>
      <button className="button" onClick={captureImage}>Capturar</button>
    </div>
  );
};

export default ImageDg;