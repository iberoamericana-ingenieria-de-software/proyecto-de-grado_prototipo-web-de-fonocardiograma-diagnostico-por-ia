import React, { useState } from 'react';
import '../estilos/CaptureSound.css';

const CaptureSound = () => {
  const [mediaRecorder, setMediaRecorder] = useState(null);
  const [isRecording, setIsRecording] = useState(false);
  const [audioUrl, setAudioUrl] = useState(null);
  const duration = 10; // Duración de grabación en segundos
  const [countdown, setCountdown] = useState(null);
  const [showControls, setShowControls] = useState(false);
  const [identification, setIdentification] = useState('');
  const [nombreCompleto, setNombreCompleto] = useState('');
  const [tipoIdentificacion, setTipoIdentificacion] = useState('CC');
  const [edad, setEdad] = useState('');
  const [sexoBiologico, setSexoBiologico] = useState('Masculino');
  const [focoAuscultatorio, setFocoAuscultatorio] = useState('Aórtico');

  const startRecording = async () => {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
      const recorder = new MediaRecorder(stream);
      const audioChunks = [];

      recorder.ondataavailable = event => {
        if (event.data.size > 0) {
          audioChunks.push(event.data);
        }
      };

      recorder.onstop = () => {
        setIsRecording(false);
        setCountdown(null);
        setIdentification('');

        const audioBlob = new Blob(audioChunks, { type: 'audio/wav' });
        const audioUrl = URL.createObjectURL(audioBlob);
        setAudioUrl(audioUrl);
        setShowControls(true);
      };

      recorder.start();
      setMediaRecorder(recorder);
      setIsRecording(true);
      setCountdown(duration);

      const interval = setInterval(() => {
        setCountdown(prevCountdown => {
          if (prevCountdown <= 0) {
            clearInterval(interval);
            recorder.stop();
            return 0;
          }
          return prevCountdown - 1;
        });
      }, 1000);

      setTimeout(() => {
        clearInterval(interval);
        recorder.stop();
      }, duration * 1000); // Duración de la grabación en milisegundos
    } catch (error) {
      console.error('Error al acceder al micrófono:', error);
    }
  };

  const stopRecording = () => {
    if (mediaRecorder && isRecording) {
      mediaRecorder.stop();
    }
  };

  const handleDownload = async () => {
    if (identification.trim() !== '') {
      const currentDate = new Date().toISOString().replace(/[-T:]/g, '').slice(0, -5);
      const formattedFileName = `${identification.trim()}_${currentDate}.wav`;
      const audioBlob = new Blob([audioUrl], { type: 'audio/wav' });

      const formData = new FormData();
      formData.append('audio', audioBlob, formattedFileName);

      const data = {
        nombreCompleto,
        tipoIdentificacion,
        identification,
        edad,
        sexoBiologico,
        focoAuscultatorio,
      };

      formData.append('data', JSON.stringify(data));

      try {
        const response = await fetch('http://127.0.0.1:5000/capture', {
          method: 'POST',
          body: formData,
        });

        if (response.ok) {
          console.log('Data saved successfully');
        } else {
          console.error('Failed to save data');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };

  return (
    <div className="CaptureSound">
      <button className="button" onClick={startRecording} disabled={isRecording}>
        Grabar/Regrabar
      </button>
      <button className="button" onClick={stopRecording} disabled={!isRecording}>
        Limpiar
      </button>
      {countdown !== null && (
        <p className="cuenta">Grabando: {countdown > 0 ? countdown : 0} segundos restantes</p>
      )}
      {showControls && (
        <div className="control">
          {audioUrl && (
            <div>
              <audio src={audioUrl} controls />
              <h3 className="h3">DATOS DEL PACIENTE</h3>
              <div>
                <form className="form">
                  <label>
                    Nombre Completo:
                    <input
                      type="text"
                      value={nombreCompleto}
                      onChange={(e) => setNombreCompleto(e.target.value)}
                    />
                  </label>
                  <label>
                    Tipo de Identificación:
                    <select value={tipoIdentificacion} onChange={(e) => setTipoIdentificacion(e.target.value)}>
                      <option value="CC">CC</option>
                      <option value="CE">CE</option>
                      <option value="PEP">PEP</option>
                      <option value="PPE">PPE</option>
                      <option value="PA">PA</option>
                      <option value="AS">AS</option>
                      <option value="MS">MS</option>
                      <option value="RC">RC</option>
                      <option value="TI">TI</option>
                      {/* Agrega más opciones según sea necesario */}
                    </select>
                  </label>
                  <label>
                    Número de identificación
                    <input
                      type="text"
                      placeholder="Número de Identificación"
                      value={identification}
                      onChange={(e) => setIdentification(e.target.value)}
                    />
                  </label>
                  <label>
                    Edad:
                    <input
                      type="number"
                      value={edad}
                      onChange={(e) => setEdad(e.target.value)}
                    />
                  </label>
                  <div>
                    Sexo Biológico:
                    <label>
                      <input
                        type="radio"
                        value="Masculino"
                        checked={sexoBiologico === 'Masculino'}
                        onChange={() => setSexoBiologico('Masculino')}
                      />
                      Masculino
                    </label>
                    <label>
                      <input
                        type="radio"
                        value="Femenino"
                        checked={sexoBiologico === 'Femenino'}
                        onChange={() => setSexoBiologico('Femenino')}
                      />
                      Femenino
                    </label>
                  </div>
                  <label>
                    Foco Auscultatorio:
                    <select value={focoAuscultatorio} onChange={(e) => setFocoAuscultatorio(e.target.value)}>
                      <option value="Aórtico">Aórtico</option>
                      <option value="Mitral">Mitral</option>
                      <option value="Tricuspideo">Tricuspídeo</option>
                      <option value="Pulmonar">Pulmonar</option>
                    </select>
                  </label>
                </form>
              </div>
              <div>
                <button className="button" onClick={handleDownload}>Descargar</button>
              </div>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default CaptureSound;