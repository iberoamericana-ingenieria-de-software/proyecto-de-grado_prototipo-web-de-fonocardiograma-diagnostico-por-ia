import React, { useEffect, useRef } from 'react';
import { Chart } from 'chart.js/auto';

const ChartSt = ({ waveformData, chartRef }) => {
  const canvasRef = useRef(null);
  const chartInstance = useRef(null);

  useEffect(() => {
    if (canvasRef.current) {
      if (chartInstance.current) {
        chartInstance.current.destroy();
      }

      const ctx = canvasRef.current.getContext('2d');

      chartInstance.current = new Chart(ctx, {
        type: 'line',
        data: {
          labels: waveformData.slice(0, 1000).map((_, index) => index.toString()), // Limitar las etiquetas a 1000 milisegundos
          datasets: [
            {
              label: 'Forma de Onda',
              data: waveformData.slice(0, 1000), // Limitar los datos a 1000 milisegundos
              borderColor: 'black',
              fill: false,
              pointRadius: 0,
            },
          ],
        },
        options: {
          scales: {
            y: {
              title: {
                display: true,
                text: 'Amplitud',
                color: 'red',
                font: {
                  size: 14,
                  family: 'Arial',
                },
              },
              ticks: {
                fontColor: 'black',
              },
            },
            x: {
              title: {
                display: true,
                text: 'Tiempo (milisegundos)',
                color: 'blue',
                font: {
                  size: 14,
                  family: 'Arial',
                },
              },
              ticks: {
                fontColor: 'blue',
              },
            },
          },
        },
      });

      // Asignar la referencia al canvas en el DOM al chartRef
      chartRef.current = canvasRef.current;
    }
  }, [waveformData, chartRef]);

  return (
    <div className="chart">
      <canvas ref={canvasRef} />
    </div>
  );
};

export default ChartSt;