import React, { useState, useEffect, useRef } from 'react';
import WaveSurfer from 'wavesurfer.js';

const WaveformDisplay = ({ audioSrc, onDataExtracted }) => {
  const [isWaveformGenerated, setIsWaveformGenerated] = useState(false);
  const waveformRef = useRef(null);

  useEffect(() => {
    const wavesurfer = WaveSurfer.create({ /*wavesurfer crea la onda graficada para poder obtener sus valores y pasarla a un plano cartesiano, pero no es necesario que se renderize, 
    sin embargo es util para verificar que funcione el componente */
      container: waveformRef.current,
      waveColor: 'white',
      progressColor: 'white',
    });

  wavesurfer.load(audioSrc);

    wavesurfer.on('ready', () => {
      if (!isWaveformGenerated) {
        wavesurfer.play();
        setIsWaveformGenerated(true);

        const data = wavesurfer.backend.getPeaks(800); /* Obtener datos de onda*/
        onDataExtracted(data);
      }
    });

    return () => {
      wavesurfer.destroy();

    };
  }, [audioSrc, isWaveformGenerated, onDataExtracted]);
/*esta onda es necesaria porque de allí se obtienen los datos de amplitud, pero no se renderiza*/
  return (
    <div >
      <div className="wave1" ref={waveformRef} />
    </div>
  );
};

export default WaveformDisplay;