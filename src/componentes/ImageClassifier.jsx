import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../estilos/ImageClassifier.css'; // Asegúrate de tener un archivo CSS para los estilos

const ImageClassifier = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [classification, setClassification] = useState('');
  const [imageFiles, setImageFiles] = useState([]);
  const [showFileList, setShowFileList] = useState(false);
  const [selectedFileName, setSelectedFileName] = useState('');

  const fetchImageFiles = async () => {
    try {
      const response = await axios.get('http://127.0.0.1:5000/imgfiles');
      setImageFiles(response.data);
    } catch (error) {
      console.error('Error al obtener archivos de imagen:', error);
    }
  };

  useEffect(() => {
    fetchImageFiles();
  }, []); // Llama a fetchImageFiles una vez al cargar el componente

  const handleImageSelect = async (imageFile) => {
    try {
      const response = await axios.get(`http://127.0.0.1:5000/imgfiles/${imageFile.nombre}`, {
        responseType: 'blob'
      });
      const file = new Blob([response.data], { type: 'image/jpeg' });
      setSelectedFile(file);
      setSelectedFileName(imageFile.nombre); // Set the selected file name
      setShowFileList(false); // Hide the file list after selection
    } catch (error) {
      console.error('Error al descargar la imagen:', error);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!selectedFile) {
      console.error('No file selected');
      return;
    }

    const formData = new FormData();
    formData.append('file', selectedFile, 'selected_image.jpg');

    try {
      const response = await axios.post('http://127.0.0.1:5000/clasificar', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      setClassification(response.data.clasificacion);
      // Después de clasificar, solicita los archivos nuevamente para actualizar la lista
      fetchImageFiles();
      setSelectedFileName(''); // Reinicia el nombre del archivo seleccionado
    } catch (error) {
      console.error('Error al clasificar la imagen:', error);
    }
  };

  return (
    <div>
      <h1>Clasificador de Imágenes</h1>
      <form onSubmit={handleSubmit}>
        <button type="button" onClick={() => setShowFileList(!showFileList)}>
          {selectedFileName ? `Seleccionado: ${selectedFileName}` : 'Elegir Archivo de Imagen'}
        </button>
        <button type="submit">Clasificar</button>
      </form>
      {showFileList && (
        <div>
          <h2>Selecciona un archivo de imagen:</h2>
          <ul>
            {imageFiles.map((imageFile, index) => (
              <li
                key={index}
                onClick={() => handleImageSelect(imageFile)}
                className={selectedFileName === imageFile.nombre ? 'selected' : ''}
              >
                {imageFile.nombre} - {new Date(imageFile.fecha_modificacion).toLocaleString()}
              </li>
            ))}
          </ul>
        </div>
      )}
      {classification && <p>Clasificación: {classification}</p>}
    </div>
  );
};

export default ImageClassifier;