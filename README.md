# Proyecto de grado_Prototipo web de fonocardiograma diagnóstico por IA

# Kardiophon lab v.1.0

Este proyecto consiste en un clasificador de imágenes y audio desarrollado con React en el frontend y Flask en el backend, Utiliza un modelo de TensorFlow para clasificar las imágenes subidas y almacena datos en una base de datos MongoDB.

## Empezar

Pasos recomendados:


## Añadir archivos

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/iberoamericana-ingenieria-de-software/proyecto-de-grado_prototipo-web-de-fonocardiograma-diagnostico-por-ia.git
git branch -M main
git push -uf origin main
```

## Integrar con otras herramientas

- [ ] [Set up project integrations](https://gitlab.com/iberoamericana-ingenieria-de-software/proyecto-de-grado_prototipo-web-de-fonocardiograma-diagnostico-por-ia/-/settings/integrations)


## Requisitos

Asegúrate de tener instalados los siguientes programas y paquetes:

- Node.js
- Python 3.x
- MongoDB
- Flask
- TensorFlow
- PIL
- OpenCV

## Instalación

### Pasos previos

### Scripts
Ingresar a la carpeta scripts en el backend, descomprimir el contenido y extraer los archivos en el directorio Scripts.

### lib
Ingresar a la carpeta lib, descomprimir el archivo site-packages de 3 partes, copiar el contenido en el directorio site-packages.

### Frontend

1. Clona el repositorio:
    ```bash
    git clone <URL_DEL_REPOSITORIO>
    cd <NOMBRE_DEL_REPOSITORIO>/frontend
    ```

2. Instala las dependencias:
    ```bash
    npm install
    ```

3. Inicia la aplicación de React:
    ```bash
    npm start
    ```

### Backend

1. Ve al directorio del backend:
    ```bash
    cd <NOMBRE_DEL_REPOSITORIO>/backend
    ```

2. Crea un entorno virtual:
    ```bash
    python -m venv env
    source env/bin/activate  # En Windows: env\Scripts\activate
    ```

3. Instala las dependencias:
    ```bash
    pip install -r requirements.txt
    ```

4. Asegúrate de que MongoDB esté en ejecución.

5. Inicia el servidor de Flask:
    ```bash
    python app.py
    ```

## Uso

1. Sube una imagen en la aplicación React frontend.
2. El backend procesará la imagen y la clasificará utilizando el modelo de TensorFlow.
3. Los resultados de la clasificación se mostrarán en el frontend.
4. Los datos se almacenarán en MongoDB.

## Rutas de la API

### `/capture` (POST)

- **Descripción**: Guarda datos de un formulario junto con un archivo de audio.
- **Parámetros**:
  - `data`: JSON con los datos del formulario.
  - `audio`: Archivo de audio en formato WAV.
- **Respuesta**: Mensaje de éxito o error.

### `/upload_image` (POST)

- **Descripción**: Sube una imagen al servidor.
- **Parámetros**:
  - `image`: Archivo de imagen en formato JPG.
- **Respuesta**: Mensaje de éxito con el nombre del archivo único generado o un mensaje de error.

### `/modelo` (GET)

- **Descripción**: Descarga el archivo del modelo TensorFlow.
- **Respuesta**: El archivo del modelo o un mensaje de error si el archivo no se encuentra.

### `/clasificar` (POST)

- **Descripción**: Clasifica una imagen usando un modelo de TensorFlow.
- **Parámetros**:
  - `file`: Archivo de imagen a clasificar.
- **Respuesta**: Clasificación de la imagen o un mensaje de error.

### `/audiofiles` (GET)

- **Descripción**: Obtiene una lista de archivos de audio en el servidor.
- **Respuesta**: Lista de archivos de audio con nombre y fecha de modificación o un mensaje de error.

### `/audiofiles/<filename>` (GET)

- **Descripción**: Descarga un archivo de audio específico.
- **Parámetros**:
  - `filename`: Nombre del archivo de audio.
- **Respuesta**: El archivo de audio o un mensaje de error.

### `/imgfiles` (GET)

- **Descripción**: Obtiene una lista de archivos de imagen en el servidor.
- **Respuesta**: Lista de archivos de imagen con nombre y fecha de modificación o un mensaje de error.

### `/imgfiles/<filename>` (GET)

- **Descripción**: Descarga un archivo de imagen específico.
- **Parámetros**:
  - `filename`: Nombre del archivo de imagen.
- **Respuesta**: El archivo de imagen o un mensaje de error.


## Soporte
Para soporte escribir directamente al correo del autor.

## Contribuciones
No se aceptan contribuaciones al proyecto particular.

## Autor
Jorge Alejandro Chamorro Ordoñez Estudiante de ingenieria de software virtual

## License
El contenido de este repositorio forma parte del trabajo de grado del mismo nombre con cesión de derechos a la corporación universitaria iberoamericana.

## Status del proyecto
Actualmente versión 1 terminada.